package ku.util;

import java.util.EmptyStackException;

public class DummyStack<T> implements Stack<T>{


	private T[] item;

	/**
	 * Constructor for creat stack with capacity.
	 * @param capacity
	 */
	public DummyStack( int capacity )
	{
		item = (T[]) new Object[capacity];
	}

	/**
	 * This method return how many item in stack.
	 * @return Number of item in stack.
	 */
	public int capacity()
	{
		int count = 0;
		for(int i=0 ; i<this.item.length ; i++)
		{
			if(this.item[i]!=null)
			{
				count++;
			}
		}
		return count;
	}

	/**
	 * Check if this stack is empty or not. If empty return true, not empty return false.
	 * @return True if empty, other return false.
	 */
	public boolean isEmpty()
	{
		for(int i=0 ; i<this.item.length ; i++)
		{
			if(this.item[i]!=null)
				return false;
		}
		return true;
	}

	/**
	 * Check is this stack is full or not.
	 * @return True if it is full, other return false;
	 */
	public boolean isFull()
	{
		for(int i=0 ; i<this.item.length ; i++)
		{
			if(this.item[i]==null)
				return false;
		}
		return true;
	}

	/**
	 * Just see what is in the top of stack.
	 * @return Top item in stack but not remove.
	 */
	public T peek()
	{
		if(this.isEmpty())
			return null;
		return item[0];
	}

	/**
	 * This will work like peek but after return it will remove that first item.
	 * @return Item at the first place in stack.
	 */
	public T pop()
	{
		if(this.isEmpty())
		{
			throw new EmptyStackException();
		}
		else
		{
			T product = this.item[0];
			this.item[0]=null;
			this.pushToTop();
			return product;
		}
	}

	/**
	 * This method call for inset something in stack.
	 * @param obj is that thing that we will put in stack.
	 */
	public void push( T obj )
	{
		if(this.isFull()){}
		else
		{
			this.pushToTop();
			T[] temp = (T[]) new Object[this.item.length];
			for(int i=0 ; i<this.item.length ; i++)
			{
				if(i<temp.length-1)
					temp[i+1]=this.item[i];
			}
			temp[0]=obj;
			this.item=temp;
		}

	}

	/**
	 * This method will count for item not size of stack.
	 * @return Number of item instack.
	 */
	public int size()
	{
		int count = 0;
		for(int i=0 ; i<this.item.length ; i++)
		{
			if(this.item[i]!=null)
				count++;
		}
		return count;
	}

	/**
	 * My own method to push all item to top by the same order
	 */
	public void pushToTop()
	{
		T[] temp = (T[])new Object[this.item.length];
		for(int i=0 ; i<temp.length ; i++)
		{
			for(int j=0 ; j<this.item.length ; j++)
			{
				if(this.item[j]!=null)
					temp[i]=item[j];
			}
		}
		this.item=temp;
		for(int i=0 ; i<temp.length ; i++)
		{
			System.out.println(temp[i]);
		}
	}

}

