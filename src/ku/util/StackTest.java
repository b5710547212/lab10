package ku.util;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StackTest {

	private Stack stack, stack2;
	/** "Before" method is run before each test. */
	@Before
	public void setUp( ) {
		StackFactory.setStackType(1);
		stack =  StackFactory.makeStack( 5 );
	}
	
	@Test( expected=IllegalArgumentException.class )
	public void newExceptionStack() {
		assertNull( StackFactory.makeStack(-1) );
	}
	
	@Test
	public void newStackIsEmpty() {
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );
	}
	
	/** pop() should throw an exception if stack is empty */
	@Test( expected=java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
		// this is unnecessary. For documentation only.
		fail("Pop empty stack should throw exception");
	}
	
	@Test
	public void testPeekEmptyStack() {
		assertNull( stack.peek() );
	}
	
	@Test
	public void testPeek() {
		stack.push("Test");
		assertEquals( "Test", stack.peek() );
		stack.pop();
	}
	
	@Test
	public void testPeekFull() {
		pushFull();
		assertEquals( "e", stack.peek() );
		emptyStack();
	}
	
	@Test
	public void testPushEmpty() {
		stack.push( new String("test") );
		assertEquals( "test", stack.pop() );
		assertTrue( stack.isEmpty() );
	}
	
	@Test
	public void testPush() {
		stack.push("test");
		stack.push("ans");
		assertEquals( "ans", stack.pop() );
		assertEquals( "test", stack.pop() );
	}
	
	@Test( expected=IllegalArgumentException.class )
	public void testPushNull() {
		stack.push(null);
	}
	
	@Test( expected= IllegalStateException.class )
	public void testPushFullStack() {
		pushFull();
		stack.push("f");
		emptyStack();
	}
	
	@Test
	public void testSizeEmpty() {
		assertEquals( 0, stack.size() );
	}
	
	@Test
	public void testSize() {
		pushFull();
		stack.pop();
		assertEquals( 4, stack.size() );
		emptyStack();
	}
	
	@Test
	public void testCapacityEmpty() {
		assertTrue( stack.isEmpty() );
	}
	
	@Test
	public void testCapacity() {
		stack.push("test");
		assertFalse( stack.isEmpty() );
		stack.pop();
	}
	
	@Test
	public void testCapacityFull() {
		pushFull();
		assertTrue( stack.isFull() );
		emptyStack();
	}
	
	@Test
	public void testIsEmptyEmpty() {
		assertTrue( stack.isEmpty() );
	}
	
	@Test
	public void testIsEmpty() {
		stack.push("test");
		assertFalse( stack.isEmpty() );
		stack.pop();
	}
	
	@Test
	public void testIsEmptyFull() {
		pushFull();
		assertFalse( stack.isEmpty() );
		emptyStack();
	}
	
	@Test
	public void testIsFullEmpty() {
		assertFalse( stack.isFull() );
	}
	
	@Test
	public void testIsFull() {
		stack.push("test");
		assertFalse( stack.isFull() );
		stack.pop();
	}
	
	@Test
	public void testIsFullFull() {
		pushFull();
		assertTrue( stack.isFull() );
		emptyStack();
	}
	
	@Test
	public void testPop() {
		pushFull();
		int init = stack.size();
		stack.pop();
		int fin = stack.size();
		assertFalse(init==fin);
	}
	
	@Test
	public void testPopOverSize() {
		pushFull();
		stack.push("z");
		assertEquals( "e", stack.pop() );
	}
	
	public void pushFull() {
		stack.push("a");
		stack.push("b");
		stack.push("c");
		stack.push("d");
		stack.push("e");
	}
	
	public void emptyStack() {
		while(!stack.isEmpty()) {
			stack.pop();
		}
	}

}
